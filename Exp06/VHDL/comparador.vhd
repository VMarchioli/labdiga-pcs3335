-- comparador

-- adição das bibliotecas necessárias
library IEEE;     
use IEEE.std_logic_1164.all;


-- entidade do comparador
-- duas entradas (A e B), cada um de dois bits, e uma saída de um bit
entity comparador is 
	port (
			A, B  : in  std_logic_vector (1 downto 0);
			igual : out STD_LOGIC
		   );
end comparador;


-- arquitetura do comparador 
-- é usado o estilo comportamental
architecture comportamental of comparador is
	begin
	
		igual <= '1' when A = B else '0'; -- descrição da saída
		
end comportamental;