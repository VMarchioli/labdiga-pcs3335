-- contador hexadecimal

-- adição das bibliotecas necessárias
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

-- entidade do contador
-- quatro entradas controladoras de um bit
-- uma entrada de quatro bits, que representa o número atual
-- uma saida de quatro bits, que representa a contagem
-- uma saida de um bit, que representa o término da contagem
entity contador is
	port (clock, zera, conta, carrega : in std_logic;
			entrada : in std_logic_vector (3 downto 0);
			contagem : out std_logic_vector (3 downto 0);
			fim : out std_logic);
	end contador;
	
	
-- arquitetura do contador com estilo comportamental
architecture comportamental of contador is
	signal IQ: integer range 0 to 15;                                    -- sinal intermediário para realizar operações
	
	begin
		
		process(clock, zera, conta, carrega, entrada, IQ)                 -- processo sensível às alterações nos sinais da lista entre parênteses
		begin
			if zera = '1' then IQ <= 0;                                    -- verificação da entrada de controle "zera"                                
			elsif clock'event and clock = '1' then                         -- verificação de subida de clock
				if carrega = '1' then                                       -- verificação da entrada de controle "carrega"
					IQ <= to_integer(unsigned(entrada));                     -- a entrada é repetida na saída
				elsif conta = '1' then                                      -- verificação da entrada de controle "conta"
					if IQ = 15 then IQ <= 0;                                 -- verifica se a contagem chegou ao fim. Caso verdadeiro, "IQ" é reiniciado
					else IQ <= IQ + 1;                                       -- "IQ" é incrementado
					end if;
				else IQ <= IQ;                                              -- mantém "IQ" caso "conta" e "carrega" forem zero 
				end if;
			end if;
		end process;
		
		contagem <= std_logic_vector(to_unsigned(IQ, contagem'length));   -- "contagem" recebe "IQ"
		
		fim <= '1' when IQ = 15 else '0';                                 -- indica o fim da contagem através do sinal "fim"
	
end comportamental;
		
			