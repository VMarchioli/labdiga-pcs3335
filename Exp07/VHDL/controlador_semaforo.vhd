library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity controlador_semaforo is
	port(
		clock, reset, PvA, PvB, NOTURNO : in std_logic;
		ViaA, ViaB                      : out std_logic_vector(2 downto 0)
	);
end entity;

architecture estrutural of controlador_semaforo is
	component logica_combinatoria IS 
	PORT
		(
			Q2 :  IN  STD_LOGIC;
			Q1 :  IN  STD_LOGIC;
			Q0 :  IN  STD_LOGIC;
			VdA :  OUT  STD_LOGIC;
			AmA :  OUT  STD_LOGIC;
			VmA :  OUT  STD_LOGIC;
			VdB :  OUT  STD_LOGIC;
			AmB :  OUT  STD_LOGIC;
			VmB :  OUT  STD_LOGIC;
			T1 :  OUT  STD_LOGIC;
			T0 :  OUT  STD_LOGIC
	);
	END component;
	
	component logica_sequencial is
	port(	
      clock, reset       : in std_logic;
		PvA, PvB, NOTURNO  : in std_logic;
		db_estado          : out std_logic_vector (2 downto 0)
		);
	end component;
	
	component circuito_temporizador is
    port (
        clock                          : in  std_logic;
		  trigger                        : in  std_logic_vector(1 downto 0);
        fim                            : out std_logic
    );
	end component;
	
	signal s_clock:  std_logic;
	signal s_estado: std_logic_vector(2 downto 0);
	signal s_trigger: std_logic_vector(1 downto 0);
begin

	sequencial: logica_sequencial port map (s_clock, reset, PvA, PvB, NOTURNO, s_estado);
	combinatorio: logica_combinatoria port map (s_estado(2), s_estado(1), s_estado(0), ViaA(2), ViaA(1), ViaA(0), ViaB(2), ViaB(1), ViaB(0), s_trigger(1), s_trigger(0));
	temporizador: circuito_temporizador port map (clock, s_trigger, s_clock);


end estrutural;




































































