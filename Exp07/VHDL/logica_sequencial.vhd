library IEEE;
use IEEE.std_logic_1164.all;

entity logica_sequencial is
	port(	
        clock, reset       : in std_logic;
		PvA, PvB, NOTURNO  : in std_logic;
		db_estado          : out std_logic_vector (2 downto 0)
		);
end logica_sequencial;

architecture comportamental of logica_sequencial is
	type Tipo_estado is (VdA_VmB, AmA_VmB, VmA_VdB, VmA_AmB, AmA_AmB, apagado);
	signal Eatual, Eprox: Tipo_estado;
begin
	--proximo estado (reset, borda do clock)
	process (reset, clock)		-- reseta ou passa pro próximo estado
	begin
		if reset = '1' then
			Eatual <= VdA_VmB;
		elsif clock'event and clock = '1' then
			Eatual <= Eprox;
		end if;
	end process;
	
	--proximo estado
	process (PvA, PvB, NOTURNO)
	begin
		case Eatual is			
			when VdA_VmB =>	if NOTURNO = '1'
								then Eprox <= AmA_AmB;
                                elsif PvB = '1' and NOTURNO = '0' 
                                then Eprox <= AmA_VmB;
                                elsif PvB = '0' and NOTURNO = '0'
                                then Eprox <= Eatual
                                end if;
			when AmA_VmB => Eprox <= VmA_VdB;
			when VmA_VdB =>  if PvA = '0' and NOTURNO = '0'
								then Eprox <= Eatual;
								else Eprox <= VmA_AmB;
								end if;
			when VmA_AmB =>	Eprox <= VdA_VmB;
            when AmA_AmB =>	if NOTURNO = '0'
                                then Eprox <= VdA_VmB;
                                else Eprox <= apagado;
                                end if;
            when apagado => Eprox <= AmA_AmB;
			when others => Eprox <= VdA_VmB;
		end case;
	end process;
    
	--saidas
	with Eatual select
		db_estado <= "000" when VdA_VmB,
					 "001" when AmA_VmB,
                     "011" when VmA_VdB,
                     "010" when VmA_AmB,
					 "100" when apagado,
					 "110" when AmA_AmB,
					 "111" when others;

end comportamental;