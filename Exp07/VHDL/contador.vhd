library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity contador is
	generic (
		limit : Integer := 50000000
	);

    port (
        clock, zera                    : in  std_logic;
        fim                            : out std_logic
    );																		
end contador;

architecture comportamental of contador is
    signal IQ: integer range 0 to limit;
begin
    process (clock, zera, IQ)
    begin
		if zera = '1' then IQ <= 0;
		elsif clock'event and clock = '1' then
			if IQ = limit-1 then IQ <= 0;
			else IQ <= IQ + 1;
			end if;
		end if;	
	end process;
   fim <= '1' when IQ = limit else '0';
end comportamental;