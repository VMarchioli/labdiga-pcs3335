-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- PROGRAM		"Quartus Prime"
-- VERSION		"Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"
-- CREATED		"Tue May 05 22:24:35 2020"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY logica_combinatoria IS 
	PORT
	(
		Q2 :  IN  STD_LOGIC;
		Q1 :  IN  STD_LOGIC;
		Q0 :  IN  STD_LOGIC;
		VdA :  OUT  STD_LOGIC;
		AmA :  OUT  STD_LOGIC;
		VmA :  OUT  STD_LOGIC;
		VdB :  OUT  STD_LOGIC;
		AmB :  OUT  STD_LOGIC;
		VmB :  OUT  STD_LOGIC;
		T1 :  OUT  STD_LOGIC;
		T0 :  OUT  STD_LOGIC;
		P1 :  OUT  STD_LOGIC;
		P0 :  OUT  STD_LOGIC
	);
END logica_combinatoria;

ARCHITECTURE bdf_type OF logica_combinatoria IS 

SIGNAL	SYNTHESIZED_WIRE_0 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_1 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_13 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_14 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_15 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_7 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_16 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_10 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_11 :  STD_LOGIC;


BEGIN 
VdA <= SYNTHESIZED_WIRE_11;
VmA <= SYNTHESIZED_WIRE_13;
VdB <= SYNTHESIZED_WIRE_10;
VmB <= SYNTHESIZED_WIRE_7;
T0 <= SYNTHESIZED_WIRE_14;
P0 <= SYNTHESIZED_WIRE_13;



SYNTHESIZED_WIRE_14 <= NOT(Q2);



SYNTHESIZED_WIRE_15 <= NOT(Q1);



AmA <= SYNTHESIZED_WIRE_0 OR SYNTHESIZED_WIRE_1;


SYNTHESIZED_WIRE_10 <= Q0 AND Q1;


P1 <= NOT(SYNTHESIZED_WIRE_13);



SYNTHESIZED_WIRE_16 <= NOT(Q0);



SYNTHESIZED_WIRE_13 <= Q1 AND SYNTHESIZED_WIRE_14;


SYNTHESIZED_WIRE_7 <= SYNTHESIZED_WIRE_14 AND SYNTHESIZED_WIRE_15;


SYNTHESIZED_WIRE_0 <= SYNTHESIZED_WIRE_15 AND Q0;


SYNTHESIZED_WIRE_11 <= SYNTHESIZED_WIRE_7 AND SYNTHESIZED_WIRE_16;


SYNTHESIZED_WIRE_1 <= Q2 AND Q1;


AmB <= Q1 AND SYNTHESIZED_WIRE_16;


T1 <= SYNTHESIZED_WIRE_10 OR SYNTHESIZED_WIRE_11;


END bdf_type;