library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity temp_seq is
	port(
		clock, reset, PvA, PvB, PpA, PpB, NOTURNO : in std_logic;
		trigger : in std_logic_vector(1 downto 0);
		estado : out std_logic_vector(2 downto 0)
	);
end entity;

architecture estrutural of temp_seq is
	
	component logica_sequencial is
	port(	
        enable, reset, clock          : in std_logic;
		  PvA, PvB, PpA, PpB, NOTURNO  : in std_logic;
		  db_estado                    : out std_logic_vector (2 downto 0)
	);
	end component;
	
	component circuito_temporizador is
    port (
        clock                          : in  std_logic;
		  trigger                        : in  std_logic_vector(1 downto 0);
        fim                            : out std_logic
    );
	end component;
	
	signal s_clock:  std_logic;
begin

	sequencial: logica_sequencial port map (s_clock, reset, clock, PvA, PvB, PpA, PpB, NOTURNO, estado);
	temporizador: circuito_temporizador port map (clock, trigger, s_clock);


end estrutural;




