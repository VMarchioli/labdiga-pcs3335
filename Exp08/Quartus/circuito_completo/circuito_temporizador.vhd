library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity circuito_temporizador is
    port (
        clock                          : in  std_logic;
		  trigger                        : in  std_logic_vector(1 downto 0);
        fim                            : out std_logic
    );
end circuito_temporizador;

architecture comportamental of circuito_temporizador is
    type maxes is array (0 to 2) of integer;
	 constant limit: maxes := (50000000, 250000000, 3000000000);
	 signal IQ: integer range 0 to 3100000000;
	 signal last_trigger: std_logic_vector(1 downto 0);
	 signal index: std_logic_vector(1 downto 0);
begin
    process (clock, index)
    begin
		if clock'event and clock = '1' then
			if index = last_trigger then
				if IQ = limit(to_integer(unsigned(index)))-1 then IQ <= 0;
				else IQ <= IQ + 1;
				end if;
			else
				IQ <= 0;
				last_trigger <= index;
			end if;
		end if;
	end process;
	
	index <= "10" when trigger = "11" else trigger;
   fim <= '1' when IQ = limit(to_integer(unsigned(index)))-1 else '0';
end comportamental;
