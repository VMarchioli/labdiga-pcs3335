-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "05/09/2020 12:48:40"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          circuito_completo
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY circuito_completo_vhd_vec_tst IS
END circuito_completo_vhd_vec_tst;
ARCHITECTURE circuito_completo_arch OF circuito_completo_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clock : STD_LOGIC;
SIGNAL NOTURNO : STD_LOGIC;
SIGNAL P0 : STD_LOGIC;
SIGNAL P1 : STD_LOGIC;
SIGNAL PpA : STD_LOGIC;
SIGNAL PpB : STD_LOGIC;
SIGNAL PvA : STD_LOGIC;
SIGNAL PvB : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL ViaA : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL ViaB : STD_LOGIC_VECTOR(2 DOWNTO 0);
COMPONENT circuito_completo
	PORT (
	clock : IN STD_LOGIC;
	NOTURNO : IN STD_LOGIC;
	P0 : BUFFER STD_LOGIC;
	P1 : BUFFER STD_LOGIC;
	PpA : IN STD_LOGIC;
	PpB : IN STD_LOGIC;
	PvA : IN STD_LOGIC;
	PvB : IN STD_LOGIC;
	reset : IN STD_LOGIC;
	ViaA : BUFFER STD_LOGIC_VECTOR(2 DOWNTO 0);
	ViaB : BUFFER STD_LOGIC_VECTOR(2 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : circuito_completo
	PORT MAP (
-- list connections between master ports and signals
	clock => clock,
	NOTURNO => NOTURNO,
	P0 => P0,
	P1 => P1,
	PpA => PpA,
	PpB => PpB,
	PvA => PvA,
	PvB => PvB,
	reset => reset,
	ViaA => ViaA,
	ViaB => ViaB
	);

-- clock
t_prcs_clock: PROCESS
BEGIN
LOOP
	clock <= '0';
	WAIT FOR 500 ps;
	clock <= '1';
	WAIT FOR 500 ps;
	IF (NOW >= 1000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_clock;

-- NOTURNO
t_prcs_NOTURNO: PROCESS
BEGIN
	NOTURNO <= '0';
WAIT;
END PROCESS t_prcs_NOTURNO;

-- reset
t_prcs_reset: PROCESS
BEGIN
	reset <= '0';
WAIT;
END PROCESS t_prcs_reset;

-- PpA
t_prcs_PpA: PROCESS
BEGIN
	PpA <= '0';
	WAIT FOR 200000 ps;
	PpA <= '1';
	WAIT FOR 10000 ps;
	PpA <= '0';
WAIT;
END PROCESS t_prcs_PpA;

-- PpB
t_prcs_PpB: PROCESS
BEGIN
	PpB <= '0';
WAIT;
END PROCESS t_prcs_PpB;

-- PvA
t_prcs_PvA: PROCESS
BEGIN
	PvA <= '1';
WAIT;
END PROCESS t_prcs_PvA;

-- PvB
t_prcs_PvB: PROCESS
BEGIN
	PvB <= '0';
WAIT;
END PROCESS t_prcs_PvB;
END circuito_completo_arch;
