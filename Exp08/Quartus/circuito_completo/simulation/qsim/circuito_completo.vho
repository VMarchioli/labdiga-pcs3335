-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

-- DATE "05/09/2020 12:48:40"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	circuito_completo IS
    PORT (
	clock : IN std_logic;
	reset : IN std_logic;
	PvA : IN std_logic;
	PvB : IN std_logic;
	PpA : IN std_logic;
	PpB : IN std_logic;
	NOTURNO : IN std_logic;
	ViaA : BUFFER std_logic_vector(2 DOWNTO 0);
	ViaB : BUFFER std_logic_vector(2 DOWNTO 0);
	P1 : BUFFER std_logic;
	P0 : BUFFER std_logic
	);
END circuito_completo;

ARCHITECTURE structure OF circuito_completo IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clock : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_PvA : std_logic;
SIGNAL ww_PvB : std_logic;
SIGNAL ww_PpA : std_logic;
SIGNAL ww_PpB : std_logic;
SIGNAL ww_NOTURNO : std_logic;
SIGNAL ww_ViaA : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_ViaB : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_P1 : std_logic;
SIGNAL ww_P0 : std_logic;
SIGNAL \PvB~input_o\ : std_logic;
SIGNAL \NOTURNO~input_o\ : std_logic;
SIGNAL \PvA~input_o\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \PpA~input_o\ : std_logic;
SIGNAL \clock~input_o\ : std_logic;
SIGNAL \PpB~input_o\ : std_logic;
SIGNAL \ViaA[0]~output_o\ : std_logic;
SIGNAL \ViaA[1]~output_o\ : std_logic;
SIGNAL \ViaA[2]~output_o\ : std_logic;
SIGNAL \ViaB[0]~output_o\ : std_logic;
SIGNAL \ViaB[1]~output_o\ : std_logic;
SIGNAL \ViaB[2]~output_o\ : std_logic;
SIGNAL \P1~output_o\ : std_logic;
SIGNAL \P0~output_o\ : std_logic;

BEGIN

ww_clock <= clock;
ww_reset <= reset;
ww_PvA <= PvA;
ww_PvB <= PvB;
ww_PpA <= PpA;
ww_PpB <= PpB;
ww_NOTURNO <= NOTURNO;
ViaA <= ww_ViaA;
ViaB <= ww_ViaB;
P1 <= ww_P1;
P0 <= ww_P0;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\ViaA[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \ViaA[0]~output_o\);

\ViaA[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \ViaA[1]~output_o\);

\ViaA[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \ViaA[2]~output_o\);

\ViaB[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \ViaB[0]~output_o\);

\ViaB[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \ViaB[1]~output_o\);

\ViaB[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \ViaB[2]~output_o\);

\P1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \P1~output_o\);

\P0~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \P0~output_o\);

\PvB~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_PvB,
	o => \PvB~input_o\);

\NOTURNO~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_NOTURNO,
	o => \NOTURNO~input_o\);

\PvA~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_PvA,
	o => \PvA~input_o\);

\reset~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

\PpA~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_PpA,
	o => \PpA~input_o\);

\clock~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clock,
	o => \clock~input_o\);

\PpB~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_PpB,
	o => \PpB~input_o\);

ww_ViaA(0) <= \ViaA[0]~output_o\;

ww_ViaA(1) <= \ViaA[1]~output_o\;

ww_ViaA(2) <= \ViaA[2]~output_o\;

ww_ViaB(0) <= \ViaB[0]~output_o\;

ww_ViaB(1) <= \ViaB[1]~output_o\;

ww_ViaB(2) <= \ViaB[2]~output_o\;

ww_P1 <= \P1~output_o\;

ww_P0 <= \P0~output_o\;
END structure;


