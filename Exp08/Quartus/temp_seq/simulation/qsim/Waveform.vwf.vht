-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "05/09/2020 11:03:37"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          temp_seq
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY temp_seq_vhd_vec_tst IS
END temp_seq_vhd_vec_tst;
ARCHITECTURE temp_seq_arch OF temp_seq_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clock : STD_LOGIC;
SIGNAL estado : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL NOTURNO : STD_LOGIC;
SIGNAL PpA : STD_LOGIC;
SIGNAL PpB : STD_LOGIC;
SIGNAL PvA : STD_LOGIC;
SIGNAL PvB : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL trigger : STD_LOGIC_VECTOR(1 DOWNTO 0);
COMPONENT temp_seq
	PORT (
	clock : IN STD_LOGIC;
	estado : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
	NOTURNO : IN STD_LOGIC;
	PpA : IN STD_LOGIC;
	PpB : IN STD_LOGIC;
	PvA : IN STD_LOGIC;
	PvB : IN STD_LOGIC;
	reset : IN STD_LOGIC;
	trigger : IN STD_LOGIC_VECTOR(1 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : temp_seq
	PORT MAP (
-- list connections between master ports and signals
	clock => clock,
	estado => estado,
	NOTURNO => NOTURNO,
	PpA => PpA,
	PpB => PpB,
	PvA => PvA,
	PvB => PvB,
	reset => reset,
	trigger => trigger
	);

-- clock
t_prcs_clock: PROCESS
BEGIN
LOOP
	clock <= '0';
	WAIT FOR 5000 ps;
	clock <= '1';
	WAIT FOR 5000 ps;
	IF (NOW >= 1000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_clock;

-- NOTURNO
t_prcs_NOTURNO: PROCESS
BEGIN
	NOTURNO <= '0';
WAIT;
END PROCESS t_prcs_NOTURNO;

-- reset
t_prcs_reset: PROCESS
BEGIN
	reset <= '0';
WAIT;
END PROCESS t_prcs_reset;

-- PpA
t_prcs_PpA: PROCESS
BEGIN
	PpA <= '0';
WAIT;
END PROCESS t_prcs_PpA;

-- PpB
t_prcs_PpB: PROCESS
BEGIN
	PpB <= '0';
WAIT;
END PROCESS t_prcs_PpB;

-- PvA
t_prcs_PvA: PROCESS
BEGIN
	PvA <= '1';
WAIT;
END PROCESS t_prcs_PvA;

-- PvB
t_prcs_PvB: PROCESS
BEGIN
	PvB <= '1';
WAIT;
END PROCESS t_prcs_PvB;
-- trigger[1]
t_prcs_trigger_1: PROCESS
BEGIN
	trigger(1) <= '0';
WAIT;
END PROCESS t_prcs_trigger_1;
-- trigger[0]
t_prcs_trigger_0: PROCESS
BEGIN
	trigger(0) <= '0';
	WAIT FOR 420000 ps;
	trigger(0) <= '1';
	WAIT FOR 560000 ps;
	trigger(0) <= '0';
WAIT;
END PROCESS t_prcs_trigger_0;
END temp_seq_arch;
