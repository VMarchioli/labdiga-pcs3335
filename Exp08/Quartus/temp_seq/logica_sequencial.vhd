library IEEE;
use IEEE.std_logic_1164.all;

entity logica_sequencial is
	port(	
        enable, reset, clock         : in std_logic;
		  PvA, PvB, PpA, PpB, NOTURNO  : in std_logic;
		  db_estado                    : out std_logic_vector (2 downto 0)
	);
end logica_sequencial;

architecture comportamental of logica_sequencial is
	type Tipo_estado is (VdA_VmB, AmA_VmB, VmA_VdB, VmA_AmB, AmA_AmB, apagado);
	signal Eatual    : Tipo_estado := Vda_VmB;
	signal Eprox     : Tipo_estado;
	signal s_PpA     : std_logic   := '0';
	signal s_PpB     : std_logic   := '0';
begin

	process (reset, enable)		
	begin
		if reset = '1' then
			Eatual <= VdA_VmB;
		elsif enable'event and enable = '1' then
			Eatual <= Eprox;
		end if;
	end process;
	
	process(PpA, PpB, clock)
	begin
		if clock'event and clock='1' then
			if (s_PpA /= '0' and s_PpA /= '1') then
				s_PpA <= '0';
			elsif PpA = '1' then 
				s_PpA <= '1';
			end if;
			
			if Eatual = VmA_VdB then
				s_PpA <= '0';
			end if;
			
			if PpB = '1' then 
					s_PpB <= '1';
			end if;
			
			if Eatual = VdA_VmB then
				s_PpB <= '0';
			end if;
		end if;
	end process;
	
	process (PvA, PvB, NOTURNO, Eatual)
	begin
		case Eatual is			
			when VdA_VmB =>  if NOTURNO = '1' and s_PpA /= '1'
								      then Eprox <= AmA_AmB;
								  elsif PvB = '0' and NOTURNO = '0' and s_PpA = '0' 
                              then Eprox <= Eatual; 
                          else Eprox <= AmA_VmB;
                          end if;
			when AmA_VmB => Eprox <= VmA_VdB;
			when VmA_VdB =>  if PvA = '0' and NOTURNO = '0' and s_PpB = '0'
									   then Eprox <= Eatual;
								  else Eprox <= VmA_AmB;
								  end if;
			when VmA_AmB => Eprox <= VdA_VmB;
         when AmA_AmB =>  if NOTURNO = '1' and s_PpA = '0' and s_PpB = '0'
                              then Eprox <= apagado;
								  elsif s_PpA = '1'
										then Eprox <= VmA_VdB;
                          else Eprox <= VdA_VmB;
                          end if;
         when apagado => Eprox <= AmA_AmB;
			when others  => Eprox <= VdA_VmB;
		end case;
	end process;
    
	with Eatual select
		db_estado <= "000" when VdA_VmB,
						 "001" when AmA_VmB,
                   "011" when VmA_VdB,
						 "010" when VmA_AmB,
						 "100" when apagado,
					    "110" when AmA_AmB,
					    "111" when others;

end comportamental;